import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  String _jeniskelamin = "";
  int _rgProgramming = -1;

  final List<Radiogroup> _jenis = [
    Radiogroup(index: 1, text: "Pria"),
    Radiogroup(index: 2, text: "Wanita"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Home   FAQ   Buat Proyek ',
          style: TextStyle(
          color: Colors.grey,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  const Text('Daftar Akun',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: 22),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 20, 8,8),
                    child: TextFormField(
                      autofocus: true,
                      decoration: InputDecoration(
                        labelText: "Email",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: "Username",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: "Password",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: "Confirm Password",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: "Nama Lengkap",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[

                            const Text("Jenis Kelamin:", textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16),),
                            _buildRadioButton(),
                          ]
                      )
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: "Asal Institusi",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: "Kontak",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(

                        child: const Text('Register'),
                        onPressed: () {
                        },
                        style: ElevatedButton.styleFrom(
                            fixedSize: const Size(500, 50),
                            primary: Colors.blue,
                            textStyle: const TextStyle(
                              color: Colors.white,
                            )
                        )
                    ),
                  ),
                ],
              ),
            ),
          ),
      ),
    );
  }

  Widget _buildRadioButton(){
    return Column(
      children: _jenis.map((programming) => RadioListTile(
        title: Text(programming.text),
        value: programming.index,
        groupValue: _rgProgramming,
        onChanged: (value){
          setState(() {
            _rgProgramming = programming.index;
            _jeniskelamin = programming.text;
          });
        },
      )).toList(),
    );
  }

}

class Radiogroup {
  final int index;
  final String text;
  Radiogroup({required this.index, required this.text});
}
