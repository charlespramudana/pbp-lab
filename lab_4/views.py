from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required


def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
    
def add_note(request):
    context = {}
    form = NoteForm(request.POST, request.FILES or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/lab-4")

    context['form']= form
    return render(request, "lab4_form.html", context)

    # Reference: https://www.geeksforgeeks.org/django-modelform-create-form-from-models/
