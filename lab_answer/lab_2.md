1. Apakah perbedaan antara JSON dan XML?
- JSON merupakan suatu format yang di-extend dari bahasa JavaScript, sementara XML diturunkan dari SGML (Standard Generalized Markup Language).
- Struktur data yang digunakan dalam JSON adalah map, sehingga terdapat pasangan key dan value, sementara struktur data yang digunakan XML adalah tree.
- Dalam penulisan, JSON tidak menggunakan tag, sementara XML menggunakan tag.
- JSON tidak mendukung penulisan comment, sementara XML memperbolehkan penulisan comment.
- Tipe data yang tersedia pada JSON meliputi string, number, object, array, boolean, dan null, sementara tipe data pada XML secara umum adalah string, namun juga mendukung tipe data seperti gambar, grafik dll.
- JSON hanya mendukung encoding UTF-8, sementara XML mendukung berbagai tipe encoding.

2. Apakah perbedaan antara HTML dan XML?
- Dari segi penulisan, HTML tidak case-sensitive, sementara XML bersifat case-sensitive
- Secara fungsi, HTML berfungsi untuk membuat kerangka dari web, sementara XML berfungsi untuk menyimpan data-data objek.
- HTML memiliki tag yang terbatas dan sudah diatur penggunaannya, sementara XML tidak terbatas dan dapat dibuat oleh pengguna.

Referensi:<br>
https://www.educba.com/json-vs-xml/<br>
https://www.geeksforgeeks.org/difference-between-json-and-xml/<br>
https://www.guru99.com/json-vs-xml-difference.html<br>
https://www.guru99.com/xml-vs-html-difference.html<br>
https://www.w3schools.com/js/js_json_datatypes.asp<br>
