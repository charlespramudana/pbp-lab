from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}
    form = FriendForm(request.POST, request.FILES or None)

    if (form.is_valid()):
        form.save()
        return HttpResponseRedirect("/lab-3")
    
    context['form']= form
    return render(request, "lab3_form.html", context)

    # Reference: https://www.geeksforgeeks.org/django-modelform-create-form-from-models/