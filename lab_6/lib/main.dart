import 'package:flutter/material.dart';

void main() {
  runApp( MaterialApp(
    theme: ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.lightBlue[800],
      fontFamily: 'Arial',

    ),
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _State createState() => _State();

}

class _State extends State<MyApp> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Home   FAQ   Buat Proyek ',
          style: TextStyle(
            color: Colors.grey,
          ),
          ),
          backgroundColor: Colors.white,
        ),
        body: Padding(
            padding: const EdgeInsets.all(30),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: const Text('Masuk ke Akun Pengguna',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 22),
                    )),
                Container(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: const Text(' Email:'),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: TextField(
                    controller: emailController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: const Text(' Password:'),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                    height: 65,
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: ElevatedButton(
                      child: const Text('Masuk'),
                      onPressed: () {
                      },
                      style:ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          textStyle: const TextStyle(
                            color: Colors.white,
                          )
                      )
                    )
                ),
                Row(
                  children: <Widget>[
                    TextButton(
                      child: const Text(
                        'Daftar disini',
                      ),
                      onPressed: () {
                        //signup screen
                      },
                    ),
                    const Text('apabila tidak memiliki akun'),
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                )
              ],
            )
        )
    );
  }
}
